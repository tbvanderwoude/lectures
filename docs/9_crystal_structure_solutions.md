---
search:
  exclude: true
---
```python tags=["initialize"]
from matplotlib import pyplot as plt 
import numpy as np
from math import pi
```
# Solutions for lecture 9 exercises

## Warm-up exercises
1. A primitive unit cell is a unit cell which contains a single lattice point. Because it only contains a lattice point, it is the unit cell with the smallest volume out of all unit cells.

2. Since the primitive unit cell should contain one lattice point, its volume is $a^3/4$.

3. Let us first consider the FCC lattice. Its primitive lattice vectors vectors are

    \begin{align*}
    \mathbf{a}_1 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{y}})\\
    \mathbf{a}_2 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{z}})\\
    \mathbf{a}_3 &= \frac{a}{2}(\mathbf{\hat{y}} + \mathbf{\hat{z}}).
    \end{align*}

    With respect to the conventional unit cell, the basis in fractional coordinates is $\bigcirc(1/2,1/2,0)$, $\bigcirc(1/2,0,1/2)$, $\bigcirc(0,1/2,1/2)$ and $\bigcirc(0,0,0)$.
    With respect to the primitive unit cell, the basis is $\bigcirc(0,0,0)$.
    Let us now consider the BCC lattice.
    The primitive lattice vectors are

    \begin{align*}
    \mathbf{a}_1 &= \frac{a}{2}(\mathbf{\hat{x}} + \mathbf{\hat{y}} + \mathbf{\hat{z}})\\
    \mathbf{a}_2 &= a\mathbf{\hat{x}}\\
    \mathbf{a}_3 &= a\mathbf{\hat{y}}.
    \end{align*}

    The basis of the conventional unit cell is $\bigcirc(0,0,0)$ and $\bigcirc(1/2,1/2,1/2)$.
    For the primitive unit cell the basis is $\bigcirc(0,0,0)$.

4. You would need at least two. A diatomic crystal could require more than two basis vectors if the atoms as determined by the spatial location of the atoms. One needs to be able to construct all atoms in the crystal using a basis plus a lattice.

5. The filling factor is

    $$
    F = \frac{\pi}{6}
    $$

6. See lecture notes

## Exercise 1: Diatomic crystal


```python
y = np.repeat(np.arange(0,8,2),4)
x = np.tile(np.arange(0,8,2),4)
plt.figure(figsize=(5,5))
plt.axis('off')

# WZ
plt.plot([5,5,7,7,5],[5,7,7,5,5], color='k',ls=':')
plt.annotate('WZ',(6,6.5),fontsize=14,ha='center')

# PUC1 
plt.plot([0,2,4,2,0],[4,6,6,4,4], color='k',ls=':')

# UPC2
plt.plot([6,4,2,4,6],[0,0,2,2,0], color='k',ls=':')

plt.plot(x,y,'ko', markersize=15)
plt.plot(x+1,y+1, 'o', markerfacecolor='none', markeredgecolor='k', markersize=15);
```

1. See plot above

2. The area of the primitive unit cell is $A = a^2$. 
If the filled and empty circles are identical particles, the nearest-neighbour distance becomes $a^* = \frac{a}{\sqrt{2}}$ and thus the area $A^* = {a^*}^2 = \frac{a^2}{2} = \frac{A}{2}$.

3. One set of primitive lattice vectors is

    $$
    \mathbf{a_1} = a \hat{\mathbf{x}}, \quad \mathbf{a_2} = a \hat{\mathbf{y}}.
    $$

    With respect to the primitive lattice vectors, the basis is

    $$
    \huge \bullet \normalsize(0,0), \quad \bigcirc(\frac{1}{2},\frac{1}{2}).
    $$

4. The lattice is a cubic lattice. The basis of the crystal is

    $$
    \huge \bullet \normalsize(0,0,0), \quad \bigcirc(\frac{1}{2},\frac{1}{2},\frac{1}{2}).
    $$

    An example of such a material is Cesium Chloride (CsCl)

5. We obtain the BCC lattice. Example: Sodium (Na)

6. The filling factor is

    $$
    F = \frac{\sqrt(3)\pi}{8}
    $$

## Exercise 2: Diamond lattice

1. The conventional unit cell of diamond consists of two intertwined fcc lattices. A possible set of primitive lattice vectors is

    \begin{align*}
    \mathbf{a_1} &= \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{y}} \right) \\
    \mathbf{a_2} &= \frac{a}{2} \left(\hat{\mathbf{x}}+\hat{\mathbf{z}} \right) \\
    \mathbf{a_3} &= \frac{a}{2} \left(\hat{\mathbf{y}}+\hat{\mathbf{z}} \right).
    \end{align*}

    The volume of the primitive unit cell is

    $$
    V = \left| \mathbf{a_1} \cdot \left(\mathbf{a_2} \times \mathbf{a_3} \right) \right| = \frac{a^3}{4}
    $$

    as expected because the conventional unit cell contains 4 lattice points.

2. The primitive unit cell contains 2 atoms. With respect to the set of primitive lattice vectors, the basis is $ \mathrm{C}(0,0,0)$ and $\mathrm{C}(\frac{1}{4},\frac{1}{4},\frac{1}{4})$.

3. The conventional unit cell of the FCC lattice contains 4 atoms.
Because the diamond conventional unit cell contains two shifted FCC lattices, it contains 8 atoms.
The volume of the conventional unit cell is $V = a^3$.

4. We indentify a nearest neighbour pair consisting of the atom at the origin and the atom at (0.25a,0.25a,0.25a). Therefore the distance between neirest neighbouring atoms is $d = \frac{\sqrt{3}a}{4}$

5. The atom density is $\rho=N_\text{atom}/a^3 = 175/$ nm$^3$. The filling factor is

    $$
    F = \frac{\sqrt{3}\pi}{16}
    $$

## Exercise 3: Directions and spacings of Miller planes

1. A Miller plane is a plane that intersects an infinite number of lattice points. Miller indices are a set of 3 integers which specify a set of parallel planes.

2. The $(hkl)$ plane intersects the lattice vectors at position vectors of $\frac{\mathbf{a_1}}{h}, \frac{\mathbf{a_2}}{k}, \frac{\mathbf{a_1}}{l}$. Can you define two vectors inside the $(hkl)$ plane that span the plane? How do you compute a vector that is orthogonal to two other vectors?

3. The procedure described in the previous subquestion also applies here.

4. We can calculate the distance between two planes by calculating the projection of any vector connecting the two planes onto a unit vector that is normal to the plane.
